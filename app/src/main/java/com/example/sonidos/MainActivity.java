package com.example.sonidos;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

import java.io.IOException;

public class MainActivity extends Activity implements View.OnClickListener {
    Button iniciar;
    Button pausar;
    Button detener;
    MediaPlayer mp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciar=(Button)findViewById(R.id.button1);
        iniciar.setOnClickListener(this);
        pausar=(Button)findViewById(R.id.button2);
        pausar.setOnClickListener(this);
        detener=(Button)findViewById(R.id.button3);
        detener.setOnClickListener(this);
        mp= MediaPlayer.create(this, R.raw.test2);
    }



    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        if(v==iniciar) {
            mp.start();
        }
        else if (v==pausar) {
            mp.pause();
        }else if (v==detener){
            mp.stop();
            try {
                mp.prepare();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}
